#默认使用latest版本，用户可根据自身软件版本进行调整
FROM tomcat:latest    
MAINTAINER HUAWEICLOUD.COM  
#把上面所选择的软件包内的文件拷贝到TOMCAT的webapps目录下
COPY ./* $CATALINA_HOME/webapps/ 
COPY ./test.sh /usr/

EXPOSE 8080
CMD ["/usr/test.sh", "run"]
                    